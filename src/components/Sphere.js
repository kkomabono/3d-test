import React, { useEffect, useRef } from "react";
import { useHelper } from "@react-three/drei";
import { BoxHelper } from "three";
import { useSphere, useCylinder } from "@react-three/cannon";

export default function Sphere({ ...props }) {
  const [sphere] = useSphere(() => ({ mass: 10, ...props }));
  useHelper(sphere, BoxHelper, "red");

  return (
    <>
      <mesh castShadow ref={sphere}>
        <sphereGeometry />
        <meshPhysicalMaterial color="yellow" />
      </mesh>
    </>
  );
}

// export default function Sphere({ ...props }) {
//   const [ref] = useSphere(() => ({ mass: 10, ...props }))
//   return (
//     <mesh ref={ref} castShadow>
//       <sphereGeometry />
//       <meshNormalMaterial />
//     </mesh>
//   )
// }
