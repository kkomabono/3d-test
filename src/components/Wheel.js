import React, { useRef, forwardRef } from "react";
import { useGLTF } from "@react-three/drei";
import { useCylinder } from "@react-three/cannon";

useGLTF.preload("/wheelDefault.glb");

const Wheel = forwardRef(({ radius = 0.7, leftSide, ...props }, ref) => {
  const { nodes, materials } = useGLTF("/wheelDefault.glb");
  useCylinder(() => ({ mass: 1, type: "Kinematic", material: "wheel", collisionFilterGroup: 0, args: [radius, radius, 0.5, 16], ...props}), ref)
  return (
    <group ref={ref} {...props} dispose={null}>
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Mesh_wheelDefault.geometry}
        material={materials.carTire}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Mesh_wheelDefault_1.geometry}
        material={materials._defaultMat}
      />
    </group>
  );
})

export default Wheel;
