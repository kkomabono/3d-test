import React from 'react';
import { usePlane } from '@react-three/cannon';

export default function PlaneModel(props) {
  const [ref] = usePlane(() => ({ rotation: [-Math.PI / 2, 0, 0], type: 'Static', material: 'ground', ...props }))
  return (
    <group ref={ref}>
      <mesh receiveShadow>
        <planeGeometry args={[500, 500]} />
        <meshStandardMaterial color="#EAA85C" />
      </mesh>
    </group>
  )
}

// export default function PlaneModel(props) {
//   const [ref] = usePlane(() => ({ rotation: [-Math.PI / 2, 0, 0], ...props }))
//   return (
//     <mesh ref={ref} receiveShadow>
//       <planeGeometry args={[1000, 1000]} />
//       <meshStandardMaterial color='#EAA85C' />
//       <shadowMaterial color="#171717" transparent opacity={0.4} />
//     </mesh>
//   )
// }