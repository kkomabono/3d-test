import React from "react";
import { usePlane } from "@react-three/cannon";

// export default function Plane(props) {
//   const [ground, api] = usePlane(() => ({ type: 'Static', mass: 1, ...props }));

//   return (
//     <group ref={ground}>
//       <mesh receiveShadow>
//         <planeGeometry attach="geometry" args={[500, 500]} />
//         <meshPhysicalMaterial attach="material" color="#bababa" />
//       </mesh>
//     </group>
//   );
// }

export default function Plane(props) {
  const [ref] = usePlane(() => ({ type: 'Static', material: 'ground', ...props }))
  return (
    <group ref={ref}>
      <mesh receiveShadow>
        <planeGeometry args={[100, 100]} />
        <meshStandardMaterial color="#bababa" />
      </mesh>
    </group>
  )
}