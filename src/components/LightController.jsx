import React, { useEffect, useRef } from "react";
import { useHelper } from "@react-three/drei";
import { DirectionalLightHelper } from "three";

export default function LightController(props) {
  const ref = useRef();
  useHelper(ref, DirectionalLightHelper, 1);

  return (
    <>
      <directionalLight
        position={[100, 50, -100]} angle={0.2} intensity={1} castShadow penumbra={1}
        ref={ref}

        shadow-mapSize-width={1024}
        shadow-mapSize-height={1024}
        shadowCameraFar={50}
      />
      <rectAreaLight
        position={[500, 80, 500]}
        intensity={7}
        width={500}
        height={500}
      />
      <rectAreaLight
        position={[-500, 80, 500]}
        intensity={7}
        width={500}
        height={500}
      />
      <ambientLight intensity={0.3} />
  </>
  );

}