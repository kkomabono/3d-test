import { Physics, usePlane, useBox } from '@react-three/cannon'

export default function Cube(props) {
  const [ref] = useBox(() => ({ mass: 1, position: [0, 5, 0], rotation: [0.4, 0.2, 0.5], ...props }))

  return (
    <mesh position={[10, 0, 0]} scale={3} ref={ref} castShadow receiveShadow>
      <boxGeometry />
      <meshLambertMaterial color="hotpink" />
    </mesh>
  )
}

// function Cube(props) {
//   const [ref] = useBox(() => ({ mass: 1, position: [0, 5, 0], rotation: [0.4, 0.2, 0.5], ...props }))
//   return (
//     <mesh receiveShadow castShadow ref={ref}>
//       <boxGeometry />
//       <meshLambertMaterial color="hotpink" />
//     </mesh>
//   )
// }