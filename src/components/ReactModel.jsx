import React, { useRef } from "react";
import { useFrame } from "@react-three/fiber";

export default function ReactModel(props) {
  const modelRef = useRef();

  useFrame(() => {
    modelRef.current.rotation.y += 0.02;
  });

  return (
    <>
      <mesh onClick={(e) => {
        e.camera.matrix.elements[2]++;
      }} position={[0, 0, 10]} ref={modelRef} castShadow scale={0.5}>
        <torusKnotBufferGeometry
          attach='geometry'
          args={[10, 1, 300, 20, 6, 10]}
        />
        <meshPhysicalMaterial
          attach='material'
          color={'#61DBFB'}
        />
      </mesh>
      <directionalLight
        castShadow
        position={[0, 10, 0]}
        intensity={4}
      />
    </>
  );
}