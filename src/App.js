import React, { useRef } from 'react';
import { Canvas } from '@react-three/fiber';
import { OrbitControls, Stats } from '@react-three/drei';
import { Stars } from '@react-three/drei';
import { Physics } from '@react-three/cannon';

import LightController from './components/LightController';
import Car from './components/Car';
import Sphere from './components/Sphere';
import Plane from './components/Plane';
import Pillar from './components/Pillar';
import Vehicle from './components/Vehicle';

function App() {

  return (
    <>
    <div style={{ width: '90vw', height: '80vh' }}>
      <Canvas
        shadows
        flat
        linear
        camera={{position: [20, 20, 20], fov: 80}}
      >
      <fog attach="fog" args={['#171720', 50, 200]} />
      <color attach="background" args={['#171720']} />
      {/* <spotLight position={[10, 10, 10]} angle={0.5} intensity={1} castShadow penumbra={1} /> */}
      <Stars />
      <LightController />
      <OrbitControls />

      <Physics gravity={[0, -9.8, 0]} broadphase="SAP" contactEquationRelaxation={4} friction={1e-3} allowSleep>
        <Plane rotation={[-Math.PI / 2, 0, 0]} userData={{ id: 'floor' }}/>
        <Vehicle position={[0, 2, 0]} rotation={[0, -Math.PI/ 4, 0]} angularVelocity={[0, 0.5, 0]} wheelRadius={0.3} />
        <Pillar position={[-20, 0, -5]} userData={{ id: 'pillar-2' }} />
        <Sphere position={[20, 20, -5]} userData={{ id: 'sphere' }}/>
      </Physics>
      </Canvas>
    </div>
    </>
  );
}

export default App;
